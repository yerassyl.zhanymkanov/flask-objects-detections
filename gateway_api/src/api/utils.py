import json
import random
import string

from werkzeug.utils import secure_filename

from ..config import UPLOAD_FOLDER
from ..aws import service as aws_service


ALPHA_NUM = string.ascii_letters + string.digits


def generate_name(length: int = 32) -> str:
    return "".join(random.choices(ALPHA_NUM, k=length))


def parse_extension(filename: str) -> str:
    return filename.split(".")[-1].lower()


def get_valid_file(request_files):
    if 'file' not in request_files:
        return

    file = request_files['file']
    if not file or not file.filename:
        return

    return file


def save_file(file):
    filename, extension = generate_name(), parse_extension(file.filename)
    filename = f"{filename}.{extension}"

    aws_service.upload_file(data=file.stream.read(), filename=filename)

    return filename


def open_object_detections(video):
    with open(f"media/object-detections/{video.detections_path}") as f:
        results = json.loads(f.read())

    return results
