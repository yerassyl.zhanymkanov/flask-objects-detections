from ..database import SessionLocal
from ..rabbitmq.producer import RabbitProducer

from .models import ObjectDetections, VideoStatus


def create_object_detections(file_path: str):
    video_detection = ObjectDetections(file_path=file_path)

    session = SessionLocal()
    session.add(video_detection)
    session.commit()
    session.refresh(video_detection)

    return video_detection.id


def get_video_object_detection(video_id: int):
    session = SessionLocal()
    return session.query(ObjectDetections).filter(ObjectDetections.id == video_id).first()


def set_detection_status(video_id: ObjectDetections, status: int):
    session = SessionLocal()
    (
        session
        .query(ObjectDetections)
        .filter(ObjectDetections.id == video_id)
        .update({"status": status}, synchronize_session="fetch")
    )
    session.commit()


def update_detected_objects(video_id: int, detections_path: str):
    session = SessionLocal()
    (
        session
        .query(ObjectDetections)
        .filter(ObjectDetections.id == video_id)
        .update(
            {
                "status": VideoStatus.DONE,
                "detections_path": detections_path,
            },
            synchronize_session="fetch"
        )
     )
    session.commit()


def publish_detection_video(video_id: int):
    rabbit_client = RabbitProducer()
    rabbit_client.publish_data({"video_id": video_id})
