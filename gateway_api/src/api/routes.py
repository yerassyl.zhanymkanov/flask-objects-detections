from flask import Blueprint, request, jsonify, abort

from ..aws import service as aws_service

from . import service
from .models import VideoStatus
from .utils import get_valid_file, save_file

bp = Blueprint('processing', __name__)


@bp.route("/videos/detected-objects", methods=["POST"])
def detected_objects():
    file = get_valid_file(request.files)
    if not file:
        return abort(400, "File is not attached or invalid")

    file_path = save_file(file)
    processing_video_id = service.create_object_detections(file_path)

    service.publish_detection_video(processing_video_id)

    return (
        jsonify({
            "id:": processing_video_id,
            "detail": "Video is accepted for processing. Please retry after a while"
        }), 202
    )


@bp.route("/videos/<int:video_id>/detected-objects", methods=["GET"])
def get_detected_objects(video_id: int):
    video = service.get_video_object_detection(video_id)

    if video.status != VideoStatus.DONE:
        return (
            jsonify({
                "id:": video.id,
                "detail": "Video is being processed. Please retry after a while"
            }), 202
        )

    results = aws_service.download_file(video.detections_path)
    return (
            jsonify({
                "id:": video.id,
                "results": results
            }), 200
    )
