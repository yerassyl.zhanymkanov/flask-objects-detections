from decouple import config

UPLOAD_FOLDER = "media"

RABBITMQ_URL = config("RABBITMQ_URL")

SQLALCHEMY_DATABASE_URI = config("DB_URI")
