from flask import Flask
from flask_cors import CORS

from .api.routes import bp as api_bp
from .database import Base, engine

Base.metadata.create_all(engine)

app = Flask(__name__)
app.register_blueprint(api_bp, url_prefix="/api")
CORS(app)

app.run(host="0.0.0.0", port=5000, debug=True)
