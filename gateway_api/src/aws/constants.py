from decouple import config

AWS_BUCKET = config("AWS_BUCKET")
AWS_REGION = config("AWS_REGION")
AWS_ACCESS_KEY = config("AWS_ACCESS_KEY")
AWS_SECRET_KEY = config("AWS_SECRET_KEY")

CONFIGS = {
    "region_name": AWS_REGION,
    "aws_secret_access_key": AWS_SECRET_KEY,
    "aws_access_key_id": AWS_ACCESS_KEY,
}

UPLOAD_FOLDER = "media/raw"
DOWNLOAD_FOLDER = "media/object-detections"

