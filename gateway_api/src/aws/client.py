import json
import logging
from functools import lru_cache

import boto3

from .constants import AWS_BUCKET, CONFIGS as AWS_CONFIGS, DOWNLOAD_FOLDER

logger = logging.getLogger("aws_logger")


class Storage:
    def __init__(self):
        self.s3 = boto3.client("s3", **AWS_CONFIGS)

    def download_json(
        self,
        *,
        key: str
    ):
        file_object = self._get_object(key=key)

        return json.load(file_object)

    def upload_file(
        self,
        *,
        data: bytes,
        filename: str,
        bucket: str = AWS_BUCKET,
    ):
        self.s3.put_object(Bucket=bucket, Key=filename, Body=data)

    def _get_object(self, *, key: str, bucket: str = AWS_BUCKET):
        return self.s3.get_object(Bucket=bucket, Key=key)["Body"]


@lru_cache
def get_storage_client() -> Storage:
    return Storage()
