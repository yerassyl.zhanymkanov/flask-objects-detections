from .client import get_storage_client


def upload_file(data: bytes, filename: str):
    storage = get_storage_client()
    return storage.upload_file(data=data, filename=filename)


def download_file(filename: str):
    storage = get_storage_client()
    return storage.download_json(key=filename)
