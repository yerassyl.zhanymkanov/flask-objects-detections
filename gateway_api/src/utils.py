import random
import string

ALPHA_NUM = string.ascii_letters + string.digits


def generate_name(length: int = 64) -> str:
    return "".join(random.choices(ALPHA_NUM, k=length))
