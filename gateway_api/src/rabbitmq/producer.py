from functools import lru_cache

from kombu import Connection, Queue

from ..config import RABBITMQ_URL


class RabbitProducer:
    def __init__(self):
        self.connection = Connection(RABBITMQ_URL)
        self.queue_name = 'objects_detections'
        self.queue = Queue(self.queue_name)

    def publish_data(self, data):
        with self.connection as conn:
            producer = conn.Producer(serializer='json')
            producer.publish(
                data,
                routing_key=self.queue_name,
                declare=[self.queue]
            )
