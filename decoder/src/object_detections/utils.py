import json
import random
import string


ALPHA_NUM = string.ascii_letters + string.digits


def generate_name(length: int = 32) -> str:
    return "".join(random.choices(ALPHA_NUM, k=length))


def parse_extension(filename: str) -> str:
    return filename.split(".")[-1].lower()


def save_detections_json(results):
    aws_filename = f"{generate_name()}.json"
    local_filepath = f"media/object-detections/{aws_filename}"

    with open(local_filepath, "w") as f:
        json.dump(results, f)

    return local_filepath, aws_filename
