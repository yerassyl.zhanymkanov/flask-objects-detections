import logging

import dramatiq

from ..config import BROKER
from ..aws import service as aws_service

from . import db_service
from .utils import save_detections_json
from .service import get_detected_objects

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

dramatiq.set_broker(BROKER)


@dramatiq.actor
def process_video(video_id: int):
    logger.info(f"Started processing video {video_id}")

    video = db_service.get_video_data(video_id)
    file_path = aws_service.download_file(video.file_path)

    detected_objects = get_detected_objects(file_path, video_id)
    local_filepath, aws_filename = save_detections_json(detected_objects)

    db_service.set_detections_json(video.id, aws_filename)
    upload_to_aws.send(local_filepath, aws_filename)

    logger.info(f"Video {video.id} has been successfully processed")


@dramatiq.actor
def upload_to_aws(local_filepath: str, aws_filename):
    logger.info(f"Sending {local_filepath} to {aws_filename}")

    aws_service.upload_file(local_filepath, aws_filename)

    logger.info(f"Sent {local_filepath} to {aws_filename}")
