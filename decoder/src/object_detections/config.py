from decouple import config
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger

logger = setup_logger()
MODEL_YAML = config("PREDICTIONS_MODEL")

FRAMES_PER_POOL = config("FRAMES_PER_POOL", default=10)


def get_configured_predictor():
    config = get_cfg()
    config_file = model_zoo.get_config_file(MODEL_YAML)

    config.merge_from_file(config_file)
    config.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
    config.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(MODEL_YAML)
    config.MODEL.DEVICE = "cpu"

    classes = MetadataCatalog.get(config.DATASETS.TRAIN[0]).thing_classes
    predictor = DefaultPredictor(config)

    logger.info("Predictor has been initialized.")

    return predictor, classes
