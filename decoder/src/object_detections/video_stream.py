# credits to https://github.com/jrosebr1/imutils

import time
from queue import Queue
from threading import Thread

import cv2


class FileVideoStream:
    def __init__(self, path, queue_size=16):
        self.stream = cv2.VideoCapture(path)
        self.stopped = False

        self.Q = Queue(maxsize=queue_size)
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True

    def start(self):
        self.thread.start()
        return self

    def update(self):
        while True:
            if self.stopped:
                break

            if self.Q.full():
                time.sleep(1)

            (grabbed, frame) = self.stream.read()

            if not grabbed:
                self.stopped = True

            self.Q.put(frame)

        self.stream.release()

    def read(self):
        return self.Q.get()

    def running(self):
        return self.more() or not self.stopped

    def more(self):
        tries = 0
        while self.Q.qsize() == 0 and not self.stopped and tries < 5:
            time.sleep(0.1)
            tries += 1

        return self.Q.qsize() > 0

    def stop(self):
        self.stopped = True
        self.thread.join()
