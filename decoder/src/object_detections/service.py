import logging
import multiprocessing as mp
import time

from .config import FRAMES_PER_POOL, get_configured_predictor
from .video_stream import FileVideoStream

logger = logging.getLogger(__name__)

NUM_PROCESSES = mp.cpu_count()
predictor, classes = get_configured_predictor()


def get_detected_objects(file_path: str, video_id: int):
    logger.info(f"Started capturing video {video_id} from {file_path}")

    fvs = FileVideoStream(file_path)
    fvs.start()

    detected_objects = []
    frames_to_process = []
    while fvs.running():
        logger.info(f"Frames collected: {len(frames_to_process)}")

        if len(frames_to_process) >= FRAMES_PER_POOL:
            chunk_detected_objects = detect_frames_objects(frames_to_process)
            detected_objects.extend(chunk_detected_objects)

            chunk_detected_objects.clear()
            frames_to_process.clear()

        if fvs.Q.empty():
            time.sleep(0.1)
            continue

        frame = fvs.read()
        frames_to_process.append(frame)

    if frames_to_process:
        chunk_detected_objects = detect_frames_objects(frames_to_process)
        detected_objects.append(chunk_detected_objects)

        chunk_detected_objects.clear()
        frames_to_process.clear()

    fvs.stop()

    return detected_objects


def detect_frames_objects(frames):
    logger.info(f"Started processing {len(frames)} frames")

    pool = mp.Pool(processes=NUM_PROCESSES)
    results = pool.map(detect_frame_objects, frames)
    pool.close()

    logger.info(f"Finished processing {len(frames)} frames")

    return results


def detect_frame_objects(frame):
    logger.info(f"Started processing the frame")

    scoring_result = predictor(frame)

    instance_fields = scoring_result["instances"].get_fields()
    scores = instance_fields["scores"].tolist()
    pred_classes = instance_fields["pred_classes"].tolist()

    frame_objects = []
    for class_idx, score in zip(pred_classes, scores):
        frame_objects.append(
            f"{classes[class_idx]}: {score}"
        )

    logger.info(f"Finished processing the frame")

    return frame_objects
