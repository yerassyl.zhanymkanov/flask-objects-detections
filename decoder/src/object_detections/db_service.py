from ..database import SessionLocal
from ..models import ObjectDetections, VideoStatus


def get_video_data(video_id: int):
    session = SessionLocal()
    return session.query(ObjectDetections).filter(ObjectDetections.id == video_id).first()


def set_detections_json(video_id: int, detections_path: str):
    session = SessionLocal()
    (
        session
        .query(ObjectDetections)
        .filter(ObjectDetections.id == video_id)
        .update(
            {
                "status": VideoStatus.DONE,
                "detections_path": detections_path,
            },
            synchronize_session="fetch"
        )
     )
    session.commit()
