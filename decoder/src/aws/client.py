import io
import logging
from functools import lru_cache

import boto3

from .constants import AWS_BUCKET, CONFIGS as AWS_CONFIGS, DOWNLOAD_FOLDER

logger = logging.getLogger("aws_logger")


class Storage:
    def __init__(self):
        self.s3 = boto3.client("s3", **AWS_CONFIGS)

    def download_file(
        self,
        *,
        key: str
    ):
        file_data = self._get_object(key=key)
        file_path = f"{DOWNLOAD_FOLDER}/{key}"

        with open(file_path, "wb") as f:
            f.write(file_data)

        return file_path

    def upload_file(
        self,
        *,
        file_path: str,
        aws_file_name: str,
        bucket: str = AWS_BUCKET,
    ):
        self.s3.upload_file(file_path, bucket, aws_file_name)

    def _get_object(self, *, key: str, bucket: str = AWS_BUCKET):
        return self.s3.get_object(Bucket=bucket, Key=key)["Body"].read()


@lru_cache
def get_storage_client() -> Storage:
    return Storage()
