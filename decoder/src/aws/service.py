import logging

from .client import get_storage_client

logger = logging.getLogger(__name__)


def upload_file(local_path: str, aws_name):
    storage = get_storage_client()
    return storage.upload_file(file_path=local_path, aws_file_name=aws_name)


def download_file(filename: str):
    logger.info(f"Starting downloading {filename}")

    storage = get_storage_client()
    file_path = storage.download_file(key=filename)

    logger.info(f"Downloaded file_path to {file_path}")

    return file_path
