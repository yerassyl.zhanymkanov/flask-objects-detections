from datetime import datetime

import sqlalchemy as sa

from .database import Base


class VideoStatus:
    NEW = 0
    DONE = 10
    FAILED = 20


class ObjectDetections(Base):
    __tablename__ = "object_detections"

    id = sa.Column(sa.Integer, primary_key=True)
    file_path = sa.Column(sa.String, unique=True, nullable=False)
    detections_path = sa.Column(sa.String, unique=True, nullable=True)
    status = sa.Column(sa.SmallInteger, default=VideoStatus.NEW, nullable=False)
    created_at = sa.Column(sa.DateTime, default=datetime.utcnow)
    updated_at = sa.Column(sa.DateTime, default=datetime.utcnow)

    @staticmethod
    def _updated_at(mapper, connection, target) -> None:
        target.updated_at = datetime.utcnow()

    @classmethod
    def __declare_last__(cls) -> None:
        sa.event.listen(cls, "before_update", cls._updated_at)
