from kombu import Queue, Connection

from .config import RABBITMQ_URL, OBJECT_DETECTIONS_QUEUE_NAME
from .rabbitmq.consumer import Worker


def run():
    queue = Queue(OBJECT_DETECTIONS_QUEUE_NAME, routing_key=OBJECT_DETECTIONS_QUEUE_NAME)

    with Connection(RABBITMQ_URL, heartbeat=4) as conn:
        worker = Worker(conn, [queue])
        worker.run()


if __name__ == '__main__':
    run()
