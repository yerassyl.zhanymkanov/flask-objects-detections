from decouple import config
from dramatiq.brokers.rabbitmq import RabbitmqBroker


RABBITMQ_URL = config("RABBITMQ_URL")
OBJECT_DETECTIONS_QUEUE_NAME = "objects_detections"

SQLALCHEMY_DATABASE_URI = config("DB_URI")
BROKER = RabbitmqBroker(url=RABBITMQ_URL)
