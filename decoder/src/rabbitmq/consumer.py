from kombu.mixins import ConsumerMixin

from ..object_detections import tasks

rabbit_url = 'amqp://guest:guest@localhost:5672//'


class Worker(ConsumerMixin):
    def __init__(self, connection, queues):
        self.connection = connection
        self.queues = queues

    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=self.queues,
                         callbacks=[self.on_message])]

    def on_message(self, body, message):
        tasks.process_video.send(body["video_id"])
        message.ack()
