## Flask API for Detectron2 objects detection

![High Level Scheme](schemas/high-level.png)
### About
Project consists of 2 services: Flask API and Video Processing services.

#### Part 1. Video Upload
1. Flask API accepts videos at endpoint
```
POST http://127.0.0.1:8001/api/videos/detected-objects (form-data, with input named "file")
```
2. Saves videos objects to AWS and video data to Postgres. 
3. API publishes `video_id` to RabbitMQ `object_detections` queue

#### Part 2. Video Processing
1. Processing services are subscribed to `object_detections` queue
2. When message is arrived, service sends `video_id` to processing workers
3. Worker downloads video from AWS, saves it locally
4. Worker opens saved video in another thread (I/O operation)
5. Worker in the main thread reads video frame by frame and starts to collect them
6. After collecting N frames (10 in this example) 
   it creates a pool with `cpu_cores` number of proccesses 
   and starts detecting objects on the frames (CPU heavy operation)
7. After processing N frames, it continues opening the video frame by frame
8. After all the processing finish, worker saves all the results from previous operations into 
   JSON file and then sends task to upload this file to AWS to other workers.
   
#### Part 3. Video Processing results
1. User requests an endpoint to get video details
```
GET `http://127.0.0.1:8001/api/videos/<int:id>/detected-objects`
```
2. If video has not finished the processing, it will inform the user about that
3. Otherwise, it will return `video_id` and `results` array with 
   detected objects on the video frame by frame


### Prerequisites
1. Docker
2. Docker-compose

### Installation
0. If your laptop has Apple Silicone CPU (M1), then export platform env
```
export DOCKER_DEFAULT_PLATFORM=linux/amd64
```
1. Set .env files
```
cp gateway_api/.env.template gateway_api/.env && cp decoder/.env.template decoder/.env
```
2. Build docker container
```
docker-compose build
```
3. Run it
```
docker-compose up
```
